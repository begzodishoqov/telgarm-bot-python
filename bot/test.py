def ask_language(chat_id, name):
    user = user_info(chat_id)
    if name:
        user.fullname = name
        user.save()
    msg = f"<b>{name}</b>\n{const.ASK_LANGUAGE}"
    rkm = ReplyKeyboardMarkup(True, row_width=2)
    rkm.add(const.LANGUAGE.get(UZ), const.LANGUAGE.get(RU))
    bot.send_message(chat_id, msg, reply_markup=rkm, parse_mode='HTML')


def ask_contact(chat_id, lang, message):
    if lang == UZ:
        msg = const.ASK_PHONE_NUMBER.get(UZ)
    elif lang == RU:
        msg = const.ASK_PHONE_NUMBER.get(RU)
    if lang == UZ:
        bsg = const.SEND_PHONE_NUM_BTN.get(UZ)
    elif lang == RU:
        bsg = const.SEND_PHONE_NUM_BTN.get(RU)

    rkm = ReplyKeyboardMarkup(True).add(KeyboardButton(bsg, request_contact=True))
    bot.send_message(chat_id, msg,  reply_markup=rkm, parse_mode='HTML')
    contact_handler(message)


def user_info(chat_id):
    return User.objects.filter(chat_id=chat_id).first()


@bot.message_handler(content_types=['contact'])
def contact_handler(message):
    chat_id = message.from_user.id
    user = user_info(chat_id)
    lang = message.from_user.language_code
    phone_num = message.contact.phone_number
    rkm = ReplyKeyboardMarkup(True, row_width=2)
    # rkm.add("Manzilni yuborish")
    user_lang = user_info(chat_id).lang

    bot.send_message(chat_id, "Lokatsiya kiriting", reply_markup=rkm)
    # location(message)
    if not user.contact_number:
        user.contact_number = phone_num
        user.save()
        location(message)
    else:
        ask_contact(chat_id, lang, message)






@bot.message_handler(commands=['del'])
def command_help(message):
    chat_id = message.from_user.id
    user = User.objects.get(chat_id=chat_id).delete()

    bot.send_message(chat_id, 'Deleted', reply_markup=ReplyKeyboardRemove())


@bot.message_handler(commands=['start'])
def command_help(message):
    chat_id = message.from_user.id
    name = message.from_user.first_name
    # Bazadan chat_id ni qidirish
    user = User.objects.filter(chat_id=chat_id)
    if not user:
        # yangi user yaratish
        id = User.objects.create(chat_id=chat_id)

    bot.set_state(message.from_user.id, UserState.language, message.chat.id)
    bot.send_message(chat_id, 'dmsada')


@bot.message_handler(state=UserState.language)
def ask_language(message):
    print('hello')
    bot.send_message(message.from_user.id, 'Olma')
@bot.message_handler(content_types=['text'])
def text_message_handler(message):
    user_name = message.from_user.first_name
    text = message.text
    chat_id = message.from_user.id
    user_lang = user_info(chat_id)
    if text == const.LANGUAGE.get(UZ) or const.LANGUAGE.get(RU):
        if text == const.LANGUAGE.get(UZ):
            lang = UZ
        elif text == const.LANGUAGE.get(RU):
            lang = RU
        else:
            ask_language(chat_id, user_name)
        if not user_lang.lang:
            user_lang.lang = lang
            user_lang.save()
            ask_contact(chat_id, lang, message)
        else:
            ask_contact(chat_id, lang, message)
            print("ask_language")


@bot.message_handler(func=lambda message: True)
def location(message):
    user_name = message.from_user.first_name
    text = message.text
    if message.text=="Manzilni yuborish":
         bot.send_location(message.from_user.id, 59.938924, 30.315311)


bot.infinity_polling()