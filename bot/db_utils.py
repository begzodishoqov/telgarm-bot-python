from users.models import TelegramUser


def create_user(chat_id, tg_user_id):
    try:
        TelegramUser.objects.create(chat_id=chat_id, tg_user_id=tg_user_id)
    except Exception as e:
        print(f"No users exists: {e}")


def get_user(chat_id):
    users = TelegramUser.objects.filter(chat_id=chat_id)
    if not users.exists():
        return None
    return users.first()


def set_user_lang(user, lang):
    user.lang = lang
    user.save()


def get_user_phone_number(phone_number):
    users = TelegramUser.objects.filter(phone_number=phone_number, chat_id__isnull=True, tg_user_id__isnull=True)
    if not users.exists():
        return None
    return users.first()