
BACK = {
    "uz": "⬅️ Orqaga",
    "ru": "⬅️ Назад",
}

WELCOME = "Assalomu Alekum fast_food botka Hush kelipsiz\nАссалому Алекум",


UZBEK = "🇺🇿 O'zbek tili"
RUSSIAN = "🇷🇺 Русский язык"

LIST_LANG = (UZBEK, RUSSIAN)


ASK_LANGUAGE = "🇷🇺 Выберите язык\n🇺🇿 Tilni tanlang"

# Phone number constants:
ASK_PHONE_NUMBER = {
    "uz": "📱  Mobil raqamni kiriting Raqamingizni taqdim etish uchun quyidagi ⬇️ tugmasini bosing.",
    "ru": "📱 Введите мобильный номер Нажмите на кнопку ниже ⬇️, чтобы поделиться своим номером.",
}

SEND_PHONE_NUM_BTN = {
    "uz": "Raqam yuborish",
    "ru": "Отправить номер",
}

ERROR = "Iltimos <br>/start<br> bosib boshidan boshlang"

