Token = '5295753057:AAGVOAPzjyxlOcqFrj45CpWmY4aMfGndsbs'

import telebot
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove
from users.models import TelegramUser
from bot import const, utils, db_utils
from telebot import custom_filters
from telebot.storage import StateMemoryStorage

state_storage = StateMemoryStorage()
bot = telebot.TeleBot(token=Token, state_storage=state_storage)


class UserState:
    start = 0
    language = 1
    contact = 2
    verification = 3
    main_menu = 4
    personal = 5
    personal_deposit_source = 7
    personal_deposit_currency = 8
    personal_deposit_bank = 9
    personal_deposit_mobile = 10
    personal_credit_type = 11


# start
def send_welcome(tg_user_id):
    bot.send_message(tg_user_id, const.WELCOME)


# tilni talang
def ask_language(chat_id):
    print("ask_dlangage")
    bot.send_message(chat_id, const.ASK_LANGUAGE, reply_markup=utils.get_buttons(const.LIST_LANG))
    bot.set_state(chat_id, UserState.language)


# telefon tanlash
def ask_contact(chat_id, lang):
    bot.send_message(chat_id, const.ASK_PHONE_NUMBER[lang], reply_markup=utils.get_phone_number_button(lang))
    bot.set_state(chat_id, UserState.contact)


# start error
def send_error(chat_id):
    bot.send_message(chat_id, const.ERROR)
    bot.set_state(chat_id, UserState.start)


def check_user(chat_id, tg_user_id):
    user = db_utils.get_user(chat_id)
    if not user:
        db_utils.create_user(chat_id, tg_user_id)
        send_welcome(tg_user_id)
        print("1til")
        ask_language(chat_id)
    elif not user.lang:
        print("2 til")
        ask_language(chat_id)
    elif not user.contact_number:
        ask_contact(chat_id, user.lang)


bot.add_custom_filter(custom_filters.StateFilter(bot))
bot.enable_saving_states()
