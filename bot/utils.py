import requests
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

from bot import const

UZ = "uz"
RU = "ru"


def get_buttons(buttons, lang=None, n=2):
    rkm = ReplyKeyboardMarkup(True, row_width=n)
    if lang is None:
        rkm.add(*(KeyboardButton(btn) for btn in buttons))
    else:
        rkm.add(*(KeyboardButton(btn[lang]) for btn in buttons))
    return rkm


def get_phone_number_button(lang):
    return ReplyKeyboardMarkup(True, row_width=1).add(
        KeyboardButton(const.ASK_PHONE_NUMBER_BTN[lang], request_contact=True),
    )

